<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = "profiles";
    protected $fillable = ['umur', 'bio'];

    // protected $attributes = [
    //     'umur' => null,
    //     'bio' => null,
    // ];
    
    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }
}
