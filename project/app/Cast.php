<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cast extends Model
{
    protected $table = "casts";
    protected $fillable = ['name', 'age', 'bio'];

    public function film(){
        return $this->belongsToMany('App\Film', 'roles', 'cast_id', 'film_id')
                    ->withPivot('name')
                    ->withTimestamps();
    }
}
