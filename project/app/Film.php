<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    protected $table = 'films';
    protected $fillable = ['title', 'summary', 'year', 'poster'];

    public function cast(){
        return $this->belongsToMany('App\Cast', 'roles', 'film_id', 'cast_id')
                    ->withPivot('name')
                    ->withTimestamps();
    }

    public function genre(){
        return $this->belongsTo('App\Genre', 'genre_id');
    }

    public function user(){
        return $this->belongsToMany('App\User', 'reviews', 'film_id', 'user_id')
                    ->withPivot('critics', 'rate')
                    ->withTimestamps();
    }
}
