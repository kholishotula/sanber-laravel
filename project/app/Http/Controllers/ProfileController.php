<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $profile = Profile::where('user_id', Auth::user()->id)->first();
        return view('profile.index', ['profile' => $profile]);
    }

    public function edit($id){
        $profile = Profile::find($id);
        return view('profile.edit', ['profile' => $profile]);
    }

    public function update($id, Request $request){
        $request->validate([
            'age' => 'required',
            'bio' => 'required',
        ]);
        
        $profile = Profile::find($id);
        $profile->umur = $request['age'];
        $profile->bio = $request['bio'];
        $profile->update();
        
        return redirect('profile')->with('success', 'Profile successfully updated!');
    }
}
