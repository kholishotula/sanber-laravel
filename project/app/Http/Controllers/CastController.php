<?php

namespace App\Http\Controllers;

use App\Cast;
use Illuminate\Http\Request;

class CastController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $casts = Cast::all();
        return view('cast.index', ['casts' => $casts]);
    }

    public function create(){
        return view('cast.create');
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required',
            'age' => 'required'
        ]);

        Cast::create([
            'name' => $request['name'],
            'age' => $request['age'],
            'bio' => $request['bio'],
        ]);

        return redirect('cast')->with('success', 'Cast successfully saved!');
    }

    public function show($id){
        $cast = Cast::find($id);
        dd($cast);

        return view('cast.show', ['cast' => $cast]);
    }

    public function edit($id){
        $cast = Cast::find($id);

        return view('cast.edit', ['cast' => $cast]);
    }

    public function update($id, Request $request){
        $request->validate([
            'name' => 'required',
            'age' => 'required'
        ]);
        
        $cast = Cast::find($id);
        $cast->name = $request['name'];
        $cast->age = $request['age'];
        $cast->bio = $request['bio'];
        $cast->update();
        
        return redirect('cast')->with('success', 'Cast successfully updated!');
    }

    public function destroy($id){
        $cast = Cast::find($id);
        $cast->delete();
        
        return redirect('cast')->with('success', 'Cast successfully deleted!');
    }
}
