<?php

namespace App\Http\Controllers;

use App\Film;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;

class ReviewController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $reviews = DB::table('reviews')
                    ->join('films', 'films.id', '=', 'reviews.film_id')
                    ->leftJoin('users', 'users.id', '=', 'reviews.user_id')
                    ->select('reviews.id', 'reviews.user_id', 'reviews.film_id', 'reviews.critics', 'reviews.rate', 'films.title', 'users.username')
                    ->get();
        return view('review.index', ['reviews' => $reviews]);
    }

    public function create(){
        $films = Film::all();
        return view('review.create', ['films' => $films]);
    }

    public function store(Request $request){
        $request->validate([
            'film' => 'required',
            'critic' => 'required',
            'rate' => 'required',
        ]);
        
        $film = Film::find($request['film']);

        $film->user()->attach(Auth::user()->id, [
            'critics' => $request['critic'],
            'rate' => $request['rate'],
        ]);

        return redirect('review')->with('success', 'Review successfully saved!');
    }

    public function show($id){
        $review = Film::find($id);
        return view('review.show', ['review' => $review]);
    }

    public function edit($id){
        $films = Film::all();
        $review = Film::find($id);
        return view('review.edit', ['films' => $films, 'review' => $review]);
    }

    public function update($id, Request $request){
        $request->validate([
            'film' => 'required',
            'critic' => 'required',
            'rate' => 'required',
        ]);
        
        DB::table('reviews')
            ->where('film_id', $id)
            ->update([
                'film_id' => $request['film'],
                'critics' => $request['critic'],
                'rate' => $request['rate'],
        ]);

        return redirect('review')->with('success', 'Review successfully updated!');
    }

    public function destroy($id){
        DB::table('reviews')
            ->where('id', $id)
            ->delete();
        
        return redirect('review')->with('success', 'Review successfully deleted!');
    }
}
