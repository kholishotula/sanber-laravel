<?php

namespace App\Http\Controllers;

use App\Film;
use App\Genre;
use App\Cast;
use Illuminate\Http\Request;

class FilmController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $films = Film::all();
        return view('film.index', ['films' => $films]);
    }

    public function create(){
        $genres = Genre::all();
        return view('film.create', ['genres' => $genres]);
    }

    public function store(Request $request){
        $request->validate([
            'title' => 'required',
            'summary' => 'required',
            'year' => 'required',
            'poster' => 'required',
            'genre' => 'required',
        ]);

        $poster = $request->file('poster');
        $poster_name = $poster->getClientOriginalName();
        
        $film = new Film([
            'title' => $request['title'],
            'summary' => $request['summary'],
            'year' => $request['year'],
            'poster' => 'images' . $poster_name,
        ]);

        $genre = Genre::find($request['genre']);
        $genre->film()->save($film);

        $cast_array = explode(',', $request['cast']);
        $cast_ids = [];
        foreach($cast_array as $cast_name){
            $cast = Cast::where('name', $cast_name)->first();
            // $cast = Cast::firstOrCreate(['name' => $cast_name]);
            $cast_ids[] = $cast->id;
        }

        $film->cast()->sync($cast_ids);

        $poster->move('images', $poster_name);

        return redirect('film')->with('success', 'Film successfully saved!');
    }

    public function show($id){
        $film = Film::find($id);
        return view('film.show', ['film' => $film]);
    }

    public function edit($id){
        $film = Film::find($id);
        $genres = Genre::all();
        return view('film.edit', ['film' => $film, 'genres' => $genres]);
    }

    public function update($id, Request $request){
        $request->validate([
            'title' => 'required',
            'summary' => 'required',
            'year' => 'required',
            'genre' => 'required',
        ]);
        
        $film = Film::find($id);
        $film->title = $request['title'];
        $film->summary = $request['summary'];
        $film->year = $request['year'];
        
        if(empty($request->file('poster'))){
            $film->poster = $film->poster;
        }
        else{
            unlink($film->poster);

            $poster = $request->file('poster');
            $poster_name = $poster->getClientOriginalName();

            $film->poster = 'images/' . $poster_name;
            $poster->move('images', $poster_name);
        }
        $film->update();

        $genre = Genre::find($request['genre']);
        $genre->film()->save($film);

        return redirect('film')->with('success', 'Film successfully updated!');
    }

    public function destroy($id){
        $film = Film::find($id);
        $film->delete();
        
        return redirect('film')->with('success', 'Film successfully deleted!');
    }
}
