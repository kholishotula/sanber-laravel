<?php

namespace App\Http\Controllers;

use App\Genre;
use Illuminate\Http\Request;

class GenreController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $genres = Genre::all();
        return view('genre.index', ['genres' => $genres]);
    }

    public function create(){
        return view('genre.create');
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required',
        ]);

        Genre::create([
            'name' => $request['name'],
        ]);

        return redirect('genre')->with('success', 'Genre successfully saved!');
    }

    public function show($id){
        $genre = Genre::find($id);

        return view('genre.show', ['genre' => $genre]);
    }

    public function edit($id){
        $genre = Genre::find($id);

        return view('genre.edit', ['genre' => $genre]);
    }

    public function update($id, Request $request){
        $request->validate([
            'name' => 'required',
        ]);
        
        $genre = Genre::find($id);
        $genre->name = $request['name'];
        $genre->update();
        
        return redirect('genre')->with('success', 'Genre successfully updated!');
    }

    public function destroy($id){
        $genre = Genre::find($id);
        $genre->delete();
        
        return redirect('genre')->with('success', 'Genre successfully deleted!');
    }
}
