@extends('layouts.master')

@section('content-header')
Edit Genre {{ $genre->id }}
@endsection

@section('content')
<!-- form start -->
<form role="form" action="/genre/{{ $genre->id }}" method="post">
    @csrf
    @method('PUT')
    <div class="card-body">
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" id="name" value="{{ old('name', $genre->name) }}" placeholder="Enter name">
            @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </div>
    <!-- /.card-body -->
</form>
@endsection