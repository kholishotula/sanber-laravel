@extends('layouts.master')

@section('content-header', 'Add New Genre')

@section('content')
<!-- form start -->
<form role="form" action="/genre" method="post">
    @csrf
    <div class="card-body">
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" id="name" value="{{ old('name', '') }}" placeholder="Enter name">
            @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Create</button>
    </div>
    <!-- /.card-body -->
</form>
@endsection