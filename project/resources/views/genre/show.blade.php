@extends('layouts.master')

@push('styles')
    <link rel="stylesheet" href="{{ asset('adminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
@endpush

@section('content-header', 'Genre Detail')

@section('content')
<a class="btn btn-success" href="/genre">Back</a>
<div class="card-body">
    <dl class="row">
        <dt class="col-sm-4">Name</dt>
        <dd class="col-sm-8">{{ $genre->name }}</dd>
    </dl>
</div>
            
            
@endsection