@extends('layouts.master')

@section('content-header', 'Add New Film')

@section('content')
<!-- form start -->
<form role="form" action="/film" method="post" enctype="multipart/form-data">
    @csrf
    <div class="card-body">
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" name="title" id="title" value="{{ old('title', '') }}" placeholder="Enter title">
            @error('title')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="summary">Summary</label><br>
            <textarea name="summary" id="summary" cols="30" rows="6">{{ old('summary', '') }}</textarea>
            @error('summary')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="year">Year</label>
            <input type="number" class="form-control" name="year" id="year" value="{{ old('year', '') }}" placeholder="Enter year">
            @error('year')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="poster">Select file for poster</label>
            <input type="file" class="form-control" name="poster" id="poster">
            @error('poster')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="genre">Genre</label>
            <select class="form-control" name="genre" id="genre">
                @foreach ($genres as $key => $genre)
                <option value="{{ $genre->id }}">{{ $genre->name }}</option>
                @endforeach
            </select>
            @error('genre')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="cast">Cast</label>
            <input type="text" class="form-control" name="cast" id="cast" value="{{ old('cast', '') }}" placeholder="Enter cast (separated by comma)">
            @error('cast')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Create</button>
    </div>
    <!-- /.card-body -->
</form>
@endsection