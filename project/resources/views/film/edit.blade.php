@extends('layouts.master')

@section('content-header')
Edit Film {{ $film->id }}
@endsection

@section('content')
<!-- form start -->
<form role="form" action="/film/{{ $film->id }}" method="post" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="card-body">
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" name="title" id="title" value="{{ old('title', $film->title) }}" placeholder="Enter title">
            @error('title')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="summary">Summary</label><br>
            <textarea name="summary" id="summary" cols="30" rows="6">{{ old('summary', $film->summary) }}</textarea>
            @error('summary')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="year">Year</label>
            <input type="number" class="form-control" name="year" id="year" value="{{ old('year', $film->year) }}" placeholder="Enter year">
            @error('year')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="old-poster">Old Poster</label><br>
            <img src="{{ asset($film->poster) }}" class="img-thumbnail" height=200px width=200px><br>
            <label for="poster">Select new file for poster</label>
            <input type="file" class="form-control" name="poster" id="poster">
        </div>
        <div class="form-group">
            <label for="genre">Genre</label>
            <select class="form-control" name="genre" id="genre">
                @foreach ($genres as $key => $genre)
                <option value="{{ $genre->id }}">{{ $genre->name }}</option>
                @endforeach
            </select>
            @error('genre')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </div>
    <!-- /.card-body -->
</form>
@endsection