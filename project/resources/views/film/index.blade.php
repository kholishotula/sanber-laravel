@extends('layouts.master')

@push('styles')
    <link rel="stylesheet" href="{{ asset('adminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
@endpush

@section('content-header', 'Films Table')

@section('content')
@if(session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
<a class="btn btn-info" href="/film/create">Add new film</a>
<table id="example2" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th style="width: 20px;">#</th>
            <th>Title</th>
            <th>Summary</th>
            <th>Year</th>
            <th>Genre</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse($films as $key => $film)
        <tr>
            <td>{{ $key + 1 }}</td>
            <td>{{ $film->title }}</td>
            <td>{{ $film->summary }}</td>
            <td>{{ $film->year }}</td>
            <td>{{ $film->genre->name }}</td>
            <td>
                <a class="btn btn-info btn-sm mr-2" href="/film/{{ $film->id }}" style="float: left;">Show</a>
                <a class="btn btn-warning btn-sm mr-2" href="/film/{{ $film->id }}/edit" style="float: left;">Edit</a>
                <form action="/film/{{ $film->id }}" method="post" class="form-inline" style="float: left;">
                    @csrf
                    @method('DELETE')
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
            </td>
        </tr>
        @empty
        <tr>
            <td colspan="12" align="center">No films</td>
        </tr>
        @endforelse
    </tbody>
</table>
@endsection

@push('scripts')
<script src="{{ asset('adminLTE/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('adminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('adminLTE/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('adminLTE/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
  $(function () {
    $("#example2").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endpush