@extends('layouts.master')

@push('styles')
    <link rel="stylesheet" href="{{ asset('adminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
@endpush

@section('content-header', 'Film Detail')

@section('content')
<a class="btn btn-success" href="/film">Back</a>
<div class="card-body">
    <dl class="row">
        <dt class="col-sm-12">
        <img src="{{ asset($film->poster) }}" class="img-thumbnail" height=200px width=200px>
        </dt>
        <dt class="col-sm-4">Title</dt>
        <dd class="col-sm-8">{{ $film->title }}</dd>
        <dt class="col-sm-4">Summary</dt>
        <dd class="col-sm-8">{{ $film->summary }}</dd>
        <dt class="col-sm-4">Year</dt>
        <dd class="col-sm-8">{{ $film->year }}</dd>
        <dt class="col-sm-4">Genre</dt>
        <dd class="col-sm-8">{{ $film->genre->name }}</dd>
        <dt class="col-sm-4">Cast</dt>
        <dd class="col-sm-8">
        @forelse ($film->cast as $cast)
            <button class="btn btn-primary btn-sm">{{ $cast->name }}</button>
        @empty
            No cast included
        @endforelse
        </dd>
    </dl>
</div>
            
            
@endsection