@extends('layouts.master')

@push('styles')
    <link rel="stylesheet" href="{{ asset('adminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
@endpush

@section('content-header', 'Cast Detail')

@section('content')
<a class="btn btn-success" href="/cast">Back</a>
<div class="card-body">
    <dl class="row">
        <dt class="col-sm-4">Name</dt>
        <dd class="col-sm-8">{{ $cast->name }}</dd>
        <dt class="col-sm-4">Age</dt>
        <dd class="col-sm-8">{{ $cast->age }}</dd>
        <dt class="col-sm-4">Bio</dt>
        <dd class="col-sm-8">{{ $cast->bio }}</dd>
    </dl>
</div>
            
            
@endsection