@extends('layouts.master')

@section('content-header', 'Add New Cast')

@section('content')
<!-- form start -->
<form role="form" action="/cast" method="post">
    @csrf
    <div class="card-body">
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" id="name" value="{{ old('name', '') }}" placeholder="Enter name">
            @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="age">Age (years)</label>
            <input type="number" class="form-control" name="age" id="age" value="{{ old('age', '') }}" placeholder="Enter age">
            @error('age')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label><br>
            <textarea name="bio" id="bio" cols="30" rows="6">{{ old('bio', '') }}</textarea>
        </div>
        <button type="submit" class="btn btn-primary">Create</button>
    </div>
    <!-- /.card-body -->
</form>
@endsection