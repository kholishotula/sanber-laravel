@extends('layouts.master')

@push('styles')
    <link rel="stylesheet" href="{{ asset('adminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
@endpush

@section('content-header', 'My Profile')

@section('content')
@if(session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
<div class="card-body">
    <dl class="row">
        <dt class="col-sm-4">Name</dt>
        <dd class="col-sm-8">{{ $profile->user->name }}</dd>
        <dt class="col-sm-4">Username</dt>
        <dd class="col-sm-8">{{ $profile->user->username }}</dd>
        <dt class="col-sm-4">Email</dt>
        <dd class="col-sm-8">{{ $profile->user->email }}</dd>
        <dt class="col-sm-4">Age</dt>
        <dd class="col-sm-8">{{ $profile->umur }}</dd>
        <dt class="col-sm-4">Bio</dt>
        <dd class="col-sm-8">{{ $profile->bio }}</dd>
    </dl>
    <a class="btn btn-warning btn-sm" href="/profile/{{ $profile->id }}/edit">Edit</a>
</div>
            
            
@endsection