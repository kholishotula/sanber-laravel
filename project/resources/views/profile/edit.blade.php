@extends('layouts.master')

@section('content-header')
Edit Profile
@endsection

@section('content')
<!-- form start -->
<form role="form" action="/profile/{{ $profile->id }}" method="post">
    @csrf
    @method('PUT')
    <div class="card-body">
        <div class="form-group">
            <label for="age">Age</label>
            <input type="number" class="form-control" name="age" id="age" value="{{ old('age', $profile->umur) }}" placeholder="Enter age">
            @error('age')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label><br>
            <textarea name="bio" id="bio" cols="30" rows="6">{{ old('bio', $profile->bio) }}</textarea>
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </div>
    <!-- /.card-body -->
</form>
@endsection