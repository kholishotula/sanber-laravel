@extends('layouts.master')

@push('styles')
    <link rel="stylesheet" href="{{ asset('adminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
@endpush

@section('content-header', 'Review Detail')

@section('content')
<a class="btn btn-success" href="/review">Back</a>
<div class="card-body">
    <dl class="row">
        <dt class="col-sm-4">Title</dt>
        <dd class="col-sm-8">{{ $review->title }}</dd>
        <dt class="col-sm-4">User</dt>
        @foreach($review->user as $review_attr)
        <dd class="col-sm-8">{{ $review_attr->username }}</dd>
        @endforeach
        <dt class="col-sm-4">Critics</dt>
        @foreach($review->user as $review_attr)
        <dd class="col-sm-8">{{ $review_attr->pivot->critics }}</dd>
        @endforeach
        <dt class="col-sm-4">Rate</dt>
        @foreach($review->user as $review_attr)
        <dd class="col-sm-8">{{ $review_attr->pivot->rate }}</dd>
        @endforeach
    </dl>
</div>
            
            
@endsection