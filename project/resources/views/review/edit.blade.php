@extends('layouts.master')

@section('content-header')
Edit Review of Movie "{{ $review->title }}"
@endsection

@section('content')
<!-- form start -->
<form role="form" action="/review/{{ $review->id }}" method="post">
    @csrf
    @method('PUT')
    <div class="card-body">
        <div class="form-group">
            <label for="film">Movie Title</label>
            <select class="form-control" name="film" id="film">
                @foreach ($films as $key => $film)
                <option value="{{ $film->id }}">{{ $film->title }}</option>
                @endforeach
            </select>
            @error('title')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="critic">Critics</label><br>
            @foreach($review->user as $review_attr)
            <textarea name="critic" id="critic" cols="30" rows="6">{{ old('critic', $review_attr->pivot->critics) }}</textarea>
            @endforeach
            @error('critic')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="rate">Rate</label>
            @foreach($review->user as $review_attr)
            <input type="number" class="form-control" name="rate" id="rate" value="{{ old('rate', $review_attr->pivot->rate) }}" placeholder="Enter rate (out of 5)">
            @endforeach
            @error('rate')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </div>
    <!-- /.card-body -->
</form>
@endsection

