@extends('layouts.master')

@section('content-header', 'Add New Film')

@section('content')
<!-- form start -->
<form role="form" action="/review" method="post">
    @csrf
    <div class="card-body">
        <div class="form-group">
            <label for="film">Movie Title</label>
            <select class="form-control" name="film" id="film">
                @foreach ($films as $key => $film)
                <option value="{{ $film->id }}">{{ $film->title }}</option>
                @endforeach
            </select>
            @error('title')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="critic">Critics</label><br>
            <textarea name="critic" id="critic" cols="30" rows="6">{{ old('critic', '') }}</textarea>
            @error('critic')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="rate">Rate</label>
            <input type="number" class="form-control" name="rate" id="rate" value="{{ old('rate', '') }}" placeholder="Enter rate (out of 5)">
            @error('rate')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Create</button>
    </div>
    <!-- /.card-body -->
</form>
@endsection