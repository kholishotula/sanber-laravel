<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', function () {
    return view('welcome');
});

// cast route
// Route::prefix('cast')->group(function () {
//     Route::get('', 'CastController@index');
//     Route::get('/create', 'CastController@create');
//     Route::post('', 'CastController@store');
//     Route::get('/{cast_id}', 'CastController@show');
//     Route::get('/{cast_id}/edit', 'CastController@edit');
//     Route::put('/{cast_id}', 'CastController@update');
//     Route::delete('/{cast_id}', 'CastController@destroy');
// });
// Penagamanan rute cast dengan middleware auth
// Route::resource('cast', 'CastController')->middleware('auth');
Route::resource('cast', 'CastController');

Route::resource('film', 'FilmController');

Route::resource('genre', 'GenreController');

Route::resource('review', 'ReviewController');

Auth::routes();

Route::resource('profile', 'ProfileController');

Route::get('/home', 'HomeController@index')->name('home');
