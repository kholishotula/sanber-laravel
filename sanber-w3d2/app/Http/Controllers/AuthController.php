<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    public function welcome(Request $request){
        $messages = [
            'required' => 'Harap isi bidang :attribute'
        ];
        
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required'
        ], $messages);

        $name = $request['first_name'] . " " . $request['last_name'];

        return view('welcome')->with('name', $name);
    }
}
